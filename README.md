![p-pi projecting.jpg](./p-pi projecting.jpg)
video : https://youtu.be/MRmD-T12HfE
# pπ

An complete, portable, low-cost computer based on the Raspberry Pi computer board and an off-the-shelf low cost LED projector. Designed for use in underprivileged organizations and schools to access digital content. 

Why? The video linked above justifies the need. 

How does it compare to other available technologies? Please see a comparison table "Comparison of Digital Ed. Hardware" in the folloing google spreadsheet:
https://docs.google.com/spreadsheets/d/1P6zLo7pqs7oaUmAdmLl2kupk8N8lk9fjKqMhi3DlPbg/edit?usp=sharing

# Brief background
The need of such a device was identified when The Door Step School, an NGO school in the city of Pune, western India, asked me to help with securing smartphones. This NGO works for the education of underprivilaged kids. The idea was to give 1 smartphone to help 5-6 kids access their remote teachers during the COVID19 pandemic. It was a tragic story to hear and to realize the extreme imbalnce of privilage. 

- Version 1.1 was just a hacked up job of a Raspberry Pi and a low cost projector. The teachers, who had never used a computer before were scared of the wires and circuit boards. My first prototype was rejected.

- Versions 1.2 was accepted for some days but it required careful handling and failed often due to non-rigid body. I also realized portability is a necessary feature. 
https://www.youtube.com/watch?v=z-25EL754_w

- Version 1.3 was completed in Jan 2022. 3 pieces were made. They will be tested on the field with teachers working with the same NGO. Fingers crossed.

# How to replicate?
In case you are interested to make one pπ for yourself, just take the laser.dxf file and get it cut and bent on a commercial service provider. There are some tricky cuts, so I fear hand cutting may not work. However, V1.3 has some issues as listed below. I speant a lot of time correcting the hardware mistakes, so V1.4 should be far better.

# Issues/comments with V1.3
- There is a long piece protruding out on the left. This was not possible to bend on CNC bend. So the laser service offered to seperate the long protruding piece as a seperate piece which can be welded later. However its welding alignment gave issues later. Need to make V1.4 easier to cut and bend.
- The V1.3 has some interferences when assembled, especially when box is closed. I did not realize them because I didn't assemble them in SolidWorks. These need to be corrected.
- Additional holes for handle, latch lock, emblem, etc were done later on the physical product and do not exist on the CAD model. V1.4 should incorporate these for ease and quickness of manufacturing.
- Spent a lot of time cutting the wires for power supply to various devices. RPi 4 has a C type USB, a costly design change. Finally I just made some direct 5 V supply to its GPIO because RPI didn't boot up when i used the regular USB-C 'fast charging' wire.
- Sound from in-built stock projectors is too poor. V1.4 must incorporate a dedicated sound amplifier and speakers into the box.
- A great suggestion from my friend Mayuresh Kulkarni - incorporate a battery to make it truely portable. In all realistic circumstances in schools for the underprivilaged, the power supply is either not functioning or too far from the place of potential use. Getting extension boards may become the bottleneck. So a 1 or 4 hour battery backup could be thought of as essential.
- Another great suggestion from my mentor Mr. Vijay Kumar, Texol, Pune. To incroporate a digital sketchpad, so that teacher can interact freehand.

# Cost and BOM for V1.3 in INR (27 Jan 2022)
For cost breakup, select the "V1.3 cost breakup" tab from the google spreadsheet.
https://docs.google.com/spreadsheets/d/1P6zLo7pqs7oaUmAdmLl2kupk8N8lk9fjKqMhi3DlPbg/edit?usp=sharing
